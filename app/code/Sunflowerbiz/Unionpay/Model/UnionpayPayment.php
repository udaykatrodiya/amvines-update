<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Sunflowerbiz\Unionpay\Model;



/**
 * Pay In Store payment method model
 */
class UnionpayPayment extends \Magento\Payment\Model\Method\AbstractMethod
{

	 const CODE       = 'unionpaypayment';
    /**
     * Payment code
     *
     * @var string
     */
    protected $_code = 'unionpaypayment';

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_isOffline = true;

	protected $_isInitializeNeeded = true;
  

}
