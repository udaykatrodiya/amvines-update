<?php

namespace Sunflowerbiz\Unionpay\Controller\Process;

class Notify extends \Magento\Framework\App\Action\Action 
{

    /**
     * @var \Magento\Quote\Model\Quote
     */
    protected $_quote = false;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var \Magento\Sales\Model\Order
     */
    protected $_order;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;
    protected $_scopeConfig;
    protected $_orderHistoryFactory;


    /**
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context
    ) {
	
		
        parent::__construct($context);
		
    }

    /**
     * Return checkout session object
     *
     * @return \Magento\Checkout\Model\Session
     */
    protected function _getCheckoutSession()
    {
        return $this->_checkoutSession;
    }

    /**
     * Set redirect
     */
    public function execute()
    {
	 $_gateway="https://gateway.95516.com/gateway/api/frontTransReq.do";
	 $_testgateway="https://101.231.204.80:5000/gateway/api/frontTransReq.do";
	 $base =str_replace(array('/app/code/Sunflowerbiz/Unionpay/Controller/Process','\app\code\Sunflowerbiz\Unionpay\Controller\Process'),'',dirname(__FILE__));
	 
		

      $this->_orderHistoryFactory =  $this->_objectManager->get('\Magento\Sales\Model\Order\Status\HistoryFactory');
	 	$merchant_id=   $this->_objectManager->create('Sunflowerbiz\Unionpay\Helper\Data')->getConfig('payment/unionpaypayment/merchant_id');
		$reqReserved= $this->_objectManager->create('Sunflowerbiz\Unionpay\Helper\Data')->getConfig('payment/unionpaypayment/reqReserved');
		$active_log_stock_update = $this->_objectManager->create('Sunflowerbiz\Unionpay\Helper\Data')->getConfig('payment/unionpaypayment/enable_log');
		$order_status_payment_accepted = $this->_objectManager->create('Sunflowerbiz\Unionpay\Helper\Data')->getConfig('payment/unionpaypayment/order_status_payment_accepted');
 		 $postData = $this->getRequest()->getPost();		
		if($active_log_stock_update){
		 $dumpFile = fopen($base .'/var/log/UnionPay.log', 'a+');
		
         fwrite($dumpFile, "\r\n".date("Y-m-d H:i:s").' : Response data from UnionPay : '."\r\n");
		 	foreach ($postData as $k => $v) {
						fwrite($dumpFile, $k.'->'.$v." ; ");
				}
		 }
		  
		 if(isset($postData['respMsg']) && $postData['respMsg']=='success' &&  $reqReserved==$postData['reqReserved'] &&  $merchant_id==$postData['merId']){
		 	$incrementId=$postData['orderId'];
				
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$order = $objectManager->create('\Magento\Sales\Model\Order')->load($incrementId); 
			$order->setState($order_status_payment_accepted)->setStatus($order_status_payment_accepted);
			$order->save();
			
            $order =$this->_getOrder($incrementId);
			$comment="Payment Done.";
			 $history = $this->_orderHistoryFactory->create()
          	 ->setStatus($order_status_payment_accepted)
            ->setComment($comment)
            ->setEntityName('order')
            ->setOrder($order)
      		 ;
			// if($_SERVER['HTTP_HOST']!='127.0.0.1')
				$history->save();
			 $this->_redirect('checkout/onepage/success');
		 }
		
	
   /*  print_r($_POST);
	 echo '<br>----<br>';
	  print_r($_GET);*/
    }

    /**
     * Get order object
     *
     * @return \Magento\Sales\Model\Order
     */
    protected function _getOrder()
    {
        if (!$this->_order) {
            $incrementId = $this->_getCheckout()->getLastRealOrderId();
            $this->_orderFactory = $this->_objectManager->get('Magento\Sales\Model\OrderFactory');
            $this->_order = $this->_orderFactory->create()->loadByIncrementId($incrementId);
        }
        return $this->_order;
    }

    /**
     * @return \Magento\Checkout\Model\Session
     */
    protected function _getCheckout()
    {
        return $this->_objectManager->get('Magento\Checkout\Model\Session');
    }

    /**
     * @return mixed
     */
    protected function _getQuote()
    {
        return $this->_objectManager->get('Magento\Quote\Model\Quote');
    }

    /**
     * @return mixed
     */
    protected function _getQuoteManagement()
    {
        return $this->_objectManager->get('\Magento\Quote\Model\QuoteManagement');
    }
}