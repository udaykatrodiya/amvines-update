<?php


namespace Sunflowerbiz\Unionpay\Block\Redirect;
use \Sunflowerbiz\Unionpay\Helper\ObjectManager as Sunflowerbiz_OM;


class Redirect extends \Magento\Framework\View\Element\Template
{


    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var  \Magento\Sales\Model\Order
     */
    protected $_order;

    /**
     * @var \Sunflowerbiz\Unionpay\Helper\Data
     */
    protected $_sunHelper;

    /**
     * @var ResolverInterface
     */
    protected $_resolver;

    /**
     * @var \Sunflowerbiz\Unionpay\Logger\SunflowerbizLogger
     */
    protected $_sunLogger;

 /**
     * Redirect constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Sunflowerbiz\Unionpay\Helper\Data $sunHelper
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = [],
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Sunflowerbiz\Unionpay\Helper\Data $sunHelper,
        \Magento\Framework\Locale\ResolverInterface $resolver
    ) {
        $this->_orderFactory = $orderFactory;
        $this->_checkoutSession = $checkoutSession;
        parent::__construct($context, $data);
        $this->_resolver = $resolver;
		 
	
		/*if($_SERVER['HTTP_HOST']=='127.0.0.1'){
		   $incrementId ='000000011';
            $this->_order = $this->_orderFactory->create()->loadByIncrementId($incrementId);
		}*/
        if (!$this->_order) {
            $incrementId = $this->_getCheckout()->getLastRealOrderId();
            $this->_order = $this->_orderFactory->create()->loadByIncrementId($incrementId);
        }
		
    }


    /**
     * @return $this
     */
    public function _prepareLayout()
    {

        return parent::_prepareLayout();
    }

    /**
     * @return string
     */
    public function getFormUrl()
    {
	
	 $_gateway="https://gateway.95516.com/gateway/api/frontTransReq.do";
	 $_testgateway="https://101.231.204.80:5000/gateway/api/frontTransReq.do";
	 $test_mode=$this->_scopeConfig->getValue('payment/unionpaypayment/test_mode', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $url = $_gateway;
       
		if($test_mode>0)
		 $url = $_testgateway;
		

        return $url;
    }

    /**
     * @return mixed
     */
    public function getPaymentMethodSelectionOnSunflowerbiz()
    {
        return $this->_sunHelper->getSunflowerbizHppConfigDataFlag('payment_selection_on_sun');
    }

    /**
     * @return array
     */
    public function getFormFields()
    {
			
        $formFields = [];
       
		
			$base =str_replace(array('/app/code/Sunflowerbiz/Unionpay/Block/Redirect','\app\code\Sunflowerbiz\Unionpay\Block\Redirect'),'',dirname(__FILE__));
		

            if ($this->_order->getPayment()) {//$this->_order->getPayment()
                $realOrderId       = $this->_order->getRealOrderId();
                $orderCurrencyCode = $this->_order->getOrderCurrencyCode();
                $amount            = $this->_order->getGrandTotal();
				$toCur = 'CNY';
				$converted_final_price=$amount;
				if($orderCurrencyCode !=$toCur){
					//	$converted_final_price=  $this->helper('Magento\Directory\Helper\Data')->currencyConvert($amount,$orderCurrencyCode,$toCur);
				}
				if($converted_final_price<=0)$converted_final_price=$amount;
	
		

	
					$merchant_id=  $this->_scopeConfig->getValue('payment/unionpaypayment/merchant_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
					$reqReserved=$this->_scopeConfig->getValue('payment/unionpaypayment/reqReserved', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
					$active_log_stock_update =$this->_scopeConfig->getValue('payment/unionpaypayment/enable_log', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
 						define('SDK_SIGN_CERT_PATH' ,$base.$this->_scopeConfig->getValue('payment/unionpaypayment/SDK_SIGN_CERT_PATH', \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
						define('SDK_SIGN_CERT_PWD' ,$this->_scopeConfig->getValue('payment/unionpaypayment/SDK_SIGN_CERT_PWD', \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
						define('SDK_ENCRYPT_CERT_PATH' , $base.$this->_scopeConfig->getValue('payment/unionpaypayment/SDK_ENCRYPT_CERT_PATH', \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
						define('SDK_VERIFY_CERT_DIR' , $base.$this->_scopeConfig->getValue('payment/unionpaypayment/SDK_VERIFY_CERT_DIR', \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
						define('SDK_FRONT_NOTIFY_URL' , $this->_urlBuilder->getUrl('unionpay/process/notify'));
						define('SDK_BACK_NOTIFY_URL' ,  $this->_urlBuilder->getUrl('unionpay/process/notify'));
			
					$Createdtime= date('YmdHis',strtotime($this->_order->getCreatedAtStoreDate()) );
					if($Createdtime=='19700101000000') $Createdtime=date('YmdHis');
					
						$params = array(
						'version' => '5.0.0',				//版本号
						'encoding' => 'utf-8',				//编码方式
						'certId' => $this->getCertId ( SDK_SIGN_CERT_PATH ),			//证书ID
						'txnType' => '01',				//交易类型	
						'txnSubType' => '01',				//交易子类
						'bizType' => '000201',				//业务类型
						'frontUrl' =>  SDK_FRONT_NOTIFY_URL,  		//前台通知地址
						'backUrl' => SDK_BACK_NOTIFY_URL,		//后台通知地址	
						'signMethod' => '01',		//签名方法
						'channelType' => '08',		//渠道类型，07-PC，08-手机
						'accessType' => '0',		//接入类型
						'merId' => $merchant_id,		        //商户代码，请改自己的测试商户号
						'orderId' => $realOrderId,	//商户订单号 date('YmdHis')
						'txnTime' =>$Createdtime,	//订单发送时间
						'txnAmt' => sprintf('%.2f', $converted_final_price)*100,		//交易金额，单位分
						'currencyCode' => '156',	//交易币种
						'defaultPayType' => '0001',	//默认支付方式	
						//'orderDesc' => '订单描述',  //订单描述，网关支付和wap支付暂时不起作用
						'reqReserved' =>$reqReserved, //请求方保留域，透传字段，查询、通知、对账文件中均会原样出现
						);
				
						$params['signature'] = $this->sign($params);
						
						
			
						
					/*	if($active_log_stock_update){
						 $dumpFile = fopen($base .'/var/log/UnionPay.log', 'a+');
						 fwrite($dumpFile, date("Y-m-d H:i:s").' : Request Data: '."\r\n");
							 foreach ($params as $k => $v) {
										fwrite($dumpFile, $k.'->'.$v." ; ");
								}
						 }*/
						 
						$fields = array();
						$sort_array = array();
						$arg = "";
						$sort_array = $this->arg_sort($params); //$parameter
						while (list ($key, $val) = each ($sort_array)) {
							$fields[$key] = $this->charset_encode($val,'utf-8');
						}
						return $fields;
            
            }

       
        return $formFields;
    }


	public function getCertId($cert_path) {
	$pkcs12certdata = file_get_contents ( $cert_path );

	openssl_pkcs12_read ( $pkcs12certdata, $certs, SDK_SIGN_CERT_PWD );
	
	$x509data = $certs ['cert'];
	openssl_x509_read ( $x509data );
	$certdata = openssl_x509_parse ( $x509data );
	$cert_id = $certdata ['serialNumber'];
	return $cert_id;
	}
    
	public function sign($params) {
		$params_str = $this->coverParamsToString ( $params );
		$params_sha1x16 = sha1 ( $params_str, FALSE );
		$cert_path = SDK_SIGN_CERT_PATH;
		$pkcs12 = file_get_contents ( $cert_path );
		openssl_pkcs12_read ( $pkcs12, $certs, SDK_SIGN_CERT_PWD );
		$private_key = $certs ['pkey'];
		$sign_falg = openssl_sign ( $params_sha1x16, $signature, $private_key, OPENSSL_ALGO_SHA1 );
		if ($sign_falg) {
		$signature_base64 = base64_encode ( $signature );
		return $signature_base64;
		}
	}
	
	public function coverParamsToString($params) {
	$sign_str = '';
	ksort ( $params );
	foreach ( $params as $key => $val ) {
		if ($key == 'signature') {
			continue;
		}
		$sign_str .= sprintf ( "%s=%s&", $key, $val );
		// $sign_str .= $key . '=' . $val . '&';
	}
	return substr ( $sign_str, 0, strlen ( $sign_str ) - 1 );
	}
    
	public function para_filter($parameter) {
		$para = array();
		while (list ($key, $val) = each ($parameter)) {
			if($key == "sign" || $key == "sign_type" || $val == "")continue;
			else	$para[$key] = $parameter[$key];

		}
		return $para;
	}
	
	public function arg_sort($array) {
		ksort($array);
		reset($array);
		return $array;
	}

	public function charset_encode($input,$_output_charset ,$_input_charset ="GBK" ) {
		$output = "";
		if($_input_charset == $_output_charset || $input ==null) {
			$output = $input;
		} elseif (function_exists("mb_convert_encoding")){
			$output = mb_convert_encoding($input,$_output_charset,$_input_charset);
		} elseif(function_exists("iconv")) {
			$output = iconv($_input_charset,$_output_charset,$input);
		} else return("sorry, you have no libs support for charset change.");
		return $output;
	}
   
	
    /**
     * Set Billing Address data
     *
     * @param $formFields
     */
    protected function setBillingAddressData($formFields)
    {
        $billingAddress = $this->_order->getBillingAddress();

        if ($billingAddress) {

            $formFields['shopper.firstName'] = trim($billingAddress->getFirstname());
            $middleName = trim($billingAddress->getMiddlename());
            if ($middleName != "") {
                $formFields['shopper.infix'] = trim($middleName);
            }

            $formFields['shopper.lastName'] = trim($billingAddress->getLastname());
            $formFields['shopper.telephoneNumber'] = trim($billingAddress->getTelephone());

            $street = $this->_sunHelper->getStreet($billingAddress);

            if (isset($street['name']) && $street['name'] != "") {
                $formFields['billingAddress.street'] = $street['name'];
            }

            if (isset($street['house_number']) && $street['house_number'] != "") {
                $formFields['billingAddress.houseNumberOrName'] = $street['house_number'];
            } else {
                $formFields['billingAddress.houseNumberOrName'] = "NA";
            }

            if (trim($billingAddress->getCity()) == "") {
                $formFields['billingAddress.city'] = "NA";
            } else {
                $formFields['billingAddress.city'] = trim($billingAddress->getCity());
            }

            if (trim($billingAddress->getPostcode()) == "") {
                $formFields['billingAddress.postalCode'] = "NA";
            } else {
                $formFields['billingAddress.postalCode'] = trim($billingAddress->getPostcode());
            }

            if (trim($billingAddress->getRegionCode()) == "") {
                $formFields['billingAddress.stateOrProvince'] = "NA";
            } else {
                $formFields['billingAddress.stateOrProvince'] = trim($billingAddress->getRegionCode());
            }

            if (trim($billingAddress->getCountryId()) == "") {
                $formFields['billingAddress.country'] = "NA";
            } else {
                $formFields['billingAddress.country'] = trim($billingAddress->getCountryId());
            }
        }
        return $formFields;
    }

    /**
     * Set Shipping Address data
     *
     * @param $formFields
     */
    protected function setShippingAddressData($formFields)
    {
        $shippingAddress = $this->_order->getShippingAddress();

        if ($shippingAddress) {

            $street = $this->_sunHelper->getStreet($shippingAddress);

            if (isset($street['name']) && $street['name'] != "") {
                $formFields['deliveryAddress.street'] = $street['name'];
            }

            if (isset($street['house_number']) && $street['house_number'] != "") {
                $formFields['deliveryAddress.houseNumberOrName'] = $street['house_number'];
            } else {
                $formFields['deliveryAddress.houseNumberOrName'] = "NA";
            }

            if (trim($shippingAddress->getCity()) == "") {
                $formFields['deliveryAddress.city'] = "NA";
            } else {
                $formFields['deliveryAddress.city'] = trim($shippingAddress->getCity());
            }

            if (trim($shippingAddress->getPostcode()) == "") {
                $formFields['deliveryAddress.postalCode'] = "NA";
            } else {
                $formFields['deliveryAddress.postalCode'] = trim($shippingAddress->getPostcode());
            }

            if (trim($shippingAddress->getRegionCode()) == "") {
                $formFields['deliveryAddress.stateOrProvince'] = "NA";
            } else {
                $formFields['deliveryAddress.stateOrProvince'] = trim($shippingAddress->getRegionCode());
            }

            if (trim($shippingAddress->getCountryId()) == "") {
                $formFields['deliveryAddress.country'] = "NA";
            } else {
                $formFields['deliveryAddress.country'] = trim($shippingAddress->getCountryId());
            }
        }
        return $formFields;
    }

    /**
     * @param $formFields
     * @return mixed
     */
    protected function setOpenInvoiceData($formFields)
    {
        $count = 0;
        $currency = $this->_order->getOrderCurrencyCode();

        foreach ($this->_order->getAllVisibleItems() as $item) {

            ++$count;
            $linename = "line".$count;
            $formFields['openinvoicedata.' . $linename . '.currencyCode'] = $currency;
            $formFields['openinvoicedata.' . $linename . '.description'] =
                str_replace("\n", '', trim($item->getName()));

            $formFields['openinvoicedata.' . $linename . '.itemAmount'] =
                $this->_sunHelper->formatAmount($item->getPrice(), $currency);

            $formFields['openinvoicedata.' . $linename . '.itemVatAmount'] =
                ($item->getTaxAmount() > 0 && $item->getPriceInclTax() > 0) ?
                    $this->_sunHelper->formatAmount(
                        $item->getPriceInclTax(),
                        $currency
                    ) -  $this->_sunHelper->formatAmount(
                        $item->getPrice(),
                        $currency
                    ) : $this->_sunHelper->formatAmount($item->getTaxAmount(), $currency);



            // $product = $item->getProduct();

            // Calculate vat percentage
            $percentageMinorUnits = $this->_sunHelper->getMinorUnitTaxPercent($item->getTaxPercent());
            $formFields['openinvoicedata.' . $linename . '.itemVatPercentage'] = $percentageMinorUnits;
            $formFields['openinvoicedata.' . $linename . '.numberOfItems'] = (int) $item->getQtyOrdered();



            if ($this->_order->getPayment()->getAdditionalInformation(
                    \Sunflowerbiz\Unionpay\Observer\SunflowerbizHppDataAssignObserver::BRAND_CODE) == "klarna"
            ) {
                $formFields['openinvoicedata.' . $linename . '.vatCategory'] = "High";
            } else {
                $formFields['openinvoicedata.' . $linename . '.vatCategory'] = "None";
            }
        }

        $formFields['openinvoicedata.refundDescription'] = "Refund / Correction for ".$formFields['merchantReference'];
        $formFields['openinvoicedata.numberOfLines'] = $count;

        return $formFields;
    }

    /**
     * @param $genderId
     * @return string
     */
    protected function getGenderText($genderId)
    {
        $result = "";
        if ($genderId == '1') {
            $result = 'MALE';
        } elseif ($genderId == '2') {
            $result = 'FEMALE';
        }
        return $result;
    }

    /**
     * @param null $date
     * @param string $format
     * @return mixed
     */
    protected function _getDate($date = null, $format = 'Y-m-d H:i:s')
    {
        if (strlen($date) < 0) {
            $date = date('d-m-Y H:i:s');
        }
        $timeStamp = new \DateTime($date);
        return $timeStamp->format($format);
    }


    /**
     * The character escape function is called from the array_map function in _signRequestParams
     *
     * @param $val
     * @return mixed
     */
    protected function escapeString($val)
    {
        return str_replace(':', '\\:', str_replace('\\', '\\\\', $val));
    }

    /**
     * Get frontend checkout session object
     *
     * @return \Magento\Checkout\Model\Session
     */
    protected function _getCheckout()
    {
        return $this->_checkoutSession;
    }
}