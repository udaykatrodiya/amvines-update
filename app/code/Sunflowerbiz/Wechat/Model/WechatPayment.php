<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Sunflowerbiz\Wechat\Model;



/**
 * Pay In Store payment method model
 */
class WechatPayment extends \Magento\Payment\Model\Method\AbstractMethod
{

	 const CODE       = 'wechatpayment';
    /**
     * Payment code
     *
     * @var string
     */
    protected $_code = 'wechatpayment';

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_isOffline = false;

protected $_isInitializeNeeded = true;

	  public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $resource,
            $resourceCollection,
            $data
        );
    }

	 public function ToUrlParams($urlObj)
    {
        $buff = "";
        foreach ($urlObj as $k => $v) {
            if ($k != "sign") {
                $buff .= $k . "=" . $v . "&";
            }
        }

        $buff = trim($buff, "&");
        return $buff;
    }
 	 private function __CreateOauthUrlForOpenid($code)
    {
		$app_id=  $this->_scopeConfig->getValue('payment/wechatpayment/app_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
     	$app_secret=  $this->_scopeConfig->getValue('payment/wechatpayment/app_secret', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
       
	   
        $urlObj["appid"] = $app_id;
        $urlObj["secret"] = $app_secret;
        $urlObj["code"] = $code;
        $urlObj["grant_type"] = "authorization_code";
        $bizString = $this->ToUrlParams($urlObj);
		//if($_SERVER['HTTP_HOST']=='127.0.0.1')  return "http://localhost/testwechatjsapi.php?getopenid=1&".$bizString;
		 
        return "https://api.weixin.qq.com/sns/oauth2/access_token?" . $bizString;
    }
	 public function validappid()
    {
        $url = $this->__CreateOauthUrlForOpenid('CODE');

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);


        $res = curl_exec($ch);
        curl_close($ch);

        $data = json_decode($res, true);
	
		 
		 $openid ='';
		if(isset($data['errmsg']) && stristr($data['errmsg'],'invalid code'))
        return true;
		else
		return false;
    }
	
  public function isActive($storeId = null)
    {
	
		$app_id=  $this->_scopeConfig->getValue('payment/wechatpayment/app_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	
		$merchant_id=  $this->_scopeConfig->getValue('payment/wechatpayment/merchant_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		$app_key=  $this->_scopeConfig->getValue('payment/wechatpayment/app_key', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		if($app_id=="" || $merchant_id=="" || $app_key=="")
		return false;
		
		//if($this->validappid())
        return (bool)(int)$this->getConfigData('active', $storeId);
		//else
		//return false;
    }

}
