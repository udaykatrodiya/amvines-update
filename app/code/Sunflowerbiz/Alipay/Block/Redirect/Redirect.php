<?php

namespace Sunflowerbiz\Alipay\Block\Redirect;
use \Sunflowerbiz\Alipay\Helper\ObjectManager as Sunflowerbiz_OM;

class Redirect extends \Magento\Framework\View\Element\Template
{


    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var  \Magento\Sales\Model\Order
     */
    protected $_order;

    /**
     * @var \Sunflowerbiz\Alipay\Helper\Data
     */
    protected $_sunHelper;

    /**
     * @var ResolverInterface
     */
    protected $_resolver;

    /**
     * @var \Sunflowerbiz\Alipay\Logger\SunflowerbizLogger
     */
    protected $_sunLogger;

 /**
     * Redirect constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Sunflowerbiz\Alipay\Helper\Data $sunHelper
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = [],
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Sunflowerbiz\Alipay\Helper\Data $sunHelper,
        \Magento\Framework\Locale\ResolverInterface $resolver
    ) {
        $this->_orderFactory = $orderFactory;
        $this->_checkoutSession = $checkoutSession;
        parent::__construct($context, $data);
		
        $this->_resolver = $resolver;
		// $this->_urlBuilder = $urlBuilder;
		
		/*if($_SERVER['HTTP_HOST']=='127.0.0.1'){
		   $incrementId ='000000011';
            $this->_order = $this->_orderFactory->create()->loadByIncrementId($incrementId);
		}*/
        if (!$this->_order) {
            $incrementId = $this->_getCheckout()->getLastRealOrderId();
            $this->_order = $this->_orderFactory->create()->loadByIncrementId($incrementId);
        }
		
    }


    /**
     * @return $this
     */
    public function _prepareLayout()
    {

        return parent::_prepareLayout();
    }

    /**
     * @return string
     */
    public function getFormUrl()
    {
	
	 $_gateway="https://mapi.alipay.com/gateway.do?";
	 $_testgateway="https://mapi.alipay.com/gateway.do?";
	 $test_mode=$this->_scopeConfig->getValue('payment/alipaypayment/test_mode', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $url = $_gateway;
       
		if($test_mode>0)
		 $url = $_testgateway;
		

        return $url;
    }

    /**
     * @return mixed
     */
    public function getPaymentMethodSelectionOnSunflowerbiz()
    {
        return $this->_sunHelper->getSunflowerbizHppConfigDataFlag('payment_selection_on_sun');
    }

    /**
     * @return array
     */
    public function getFormFields()
    {
			
        $formFields = [];
      
		
			$base =str_replace(array('/app/code/Sunflowerbiz/Alipay/Block/Redirect','\app\code\Sunflowerbiz\Alipay\Block\Redirect'),'',dirname(__FILE__));


            if ($this->_order->getPayment()) {//$this->_order->getPayment()
                $realOrderId       = $this->_order->getRealOrderId();
                $orderCurrencyCode = $this->_order->getOrderCurrencyCode();
                $amount            = $this->_order->getGrandTotal();
				$toCur = 'CNY';
				$converted_final_price=$amount;
				if($orderCurrencyCode !=$toCur){
					//	$converted_final_price=  $this->helper('Magento\Directory\Helper\Data')->currencyConvert($amount,$orderCurrencyCode,$toCur);
				}
				if($converted_final_price<=0)$converted_final_price=$amount;
	
		
					$active_log_stock_update =$this->_scopeConfig->getValue('payment/alipaypayment/enable_log', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
 
						define('SDK_FRONT_NOTIFY_URL' , $this->_urlBuilder->getUrl('alipay/process/notify'));
						define('SDK_FRONT_RETURN_URL' , $this->_urlBuilder->getUrl('alipay/process/back'));
						define('SDK_BACK_NOTIFY_URL' ,  $this->_urlBuilder->getUrl('alipay/process/error'));
						
						
					$partner_id=  $this->_scopeConfig->getValue('payment/alipaypayment/partner_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
					$security_code=  $this->_scopeConfig->getValue('payment/alipaypayment/security_code', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
					$seller_email=  $this->_scopeConfig->getValue('payment/alipaypayment/seller_email', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
					
					
			
					$Createdtime= date('YmdHis',strtotime($this->_order->getCreatedAtStoreDate()) );
					if($Createdtime=='19700101000000') $Createdtime=date('YmdHis');
					
						
						
						
							$parameter = array('service'           => 'create_direct_pay_by_user',
							   'partner'           => $partner_id,
							   'return_url'        => SDK_FRONT_RETURN_URL,
							   'notify_url'        => SDK_FRONT_NOTIFY_URL,
							   '_input_charset'    => 'utf-8',
							   'subject'           => $realOrderId, 
							   'body'              =>  $realOrderId, 
							   'out_trade_no'      =>  $realOrderId,  // order ID
							   'logistics_fee'     => '0.00', //because magento has shipping system, it has included shipping price
							   'logistics_payment' => 'BUYER_PAY',  //always
							   'logistics_type'    => 'EXPRESS', //Only three shipping method:POST,EMS,EXPRESS
							   'price'             => sprintf('%.2f', $converted_final_price) ,
							   'payment_type'      => '1',
							   'quantity'          => '1', // For the moment, the parameter of price is total price, so the quantity is 1.
							   'show_url'          => $this->_urlBuilder->getUrl(),
							   //'app_pay'      => 'Y',
							   'seller_email'      => $seller_email
							);
							
			if($this->is_mobile_request()){
			
			$parameter = array('service'           => 'alipay.wap.create.direct.pay.by.user',
							   'partner'           => $partner_id,
							   'seller_id'           => $partner_id,
							   'return_url'        =>SDK_FRONT_RETURN_URL,
							   'notify_url'        => SDK_FRONT_NOTIFY_URL,
							   '_input_charset'    => 'utf-8',
							   'subject'           => $realOrderId, 
							   'out_trade_no'      => $realOrderId, // order ID
							   'total_fee'             => sprintf('%.2f', $converted_final_price) ,
							   'payment_type'      => '1',
							   'app_pay'      => 'Y'
							);
							
							 
		}
		
				
					/*	if($active_log_stock_update){
						 $dumpFile = fopen($base .'/var/log/AliPay.log', 'a+');
						 fwrite($dumpFile, date("Y-m-d H:i:s").' : Request Data: '."\r\n");
							 foreach ($params as $k => $v) {
										fwrite($dumpFile, $k.'->'.$v." ; ");
								}
						 }*/
						
		$sign_type = 'MD5';
		$parameter = $this->para_filter($parameter);	
		$sort_array = array();
		$arg = "";
		$sort_array = $this->arg_sort($parameter); //$parameter
		
		while (list ($key, $val) = each ($sort_array)) {
			$arg.=$key."=".$this->charset_encode($val,$parameter['_input_charset'])."&";
		}
		
		$prestr = substr($arg,0,count($arg)-2);
		
		$mysign = $this->sign($prestr.$security_code);
		
		$fields = array();
		$sort_array = array();
		$arg = "";
		$sort_array = $this->arg_sort($parameter); //$parameter
		while (list ($key, $val) = each ($sort_array)) {
			$fields[$key] = $this->charset_encode($val,'utf-8');
		}
		$fields['sign'] = $mysign;
		$fields['sign_type'] = $sign_type;
						return $fields;
            
            }

      
        return $formFields;
    }

 public function is_mobile_request()  
{  
 $_SERVER['ALL_HTTP'] = isset($_SERVER['ALL_HTTP']) ? $_SERVER['ALL_HTTP'] : '';  
 $mobile_browser = '0';  
 if(preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|iphone|ipad|ipod|android|xoom)/i', strtolower($_SERVER['HTTP_USER_AGENT'])))  
  $mobile_browser++;  
 if((isset($_SERVER['HTTP_ACCEPT'])) and (strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') !== false))  
  $mobile_browser++;  
 if(isset($_SERVER['HTTP_X_WAP_PROFILE']))  
  $mobile_browser++;  
 if(isset($_SERVER['HTTP_PROFILE']))  
  $mobile_browser++;  
 $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'],0,4));  
 $mobile_agents = array(  
    'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',  
    'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',  
    'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',  
    'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',  
    'newt','noki','oper','palm','pana','pant','phil','play','port','prox',  
    'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',  
    'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',  
    'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',  
    'wapr','webc','winw','winw','xda','xda-'
    );  
 if(in_array($mobile_ua, $mobile_agents))  
  $mobile_browser++;  
 if(strpos(strtolower($_SERVER['ALL_HTTP']), 'operamini') !== false)  
  $mobile_browser++;  
 // Pre-final check to reset everything if the user is on Windows  
 if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'windows') !== false)  
  $mobile_browser=0;  
 // But WP7 is also Windows, with a slightly different characteristic  
 if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'windows phone') !== false)  
  $mobile_browser++;  
 if($mobile_browser>0)  
  return true;  
 else
  return false;
  }
    
	public function sign($prestr) {
		$mysign = md5($prestr);
		return $mysign;
	}
    
	
	public function coverParamsToString($params) {
	$sign_str = '';
	ksort ( $params );
	foreach ( $params as $key => $val ) {
		if ($key == 'signature') {
			continue;
		}
		$sign_str .= sprintf ( "%s=%s&", $key, $val );
		// $sign_str .= $key . '=' . $val . '&';
	}
	return substr ( $sign_str, 0, strlen ( $sign_str ) - 1 );
	}
    
	public function para_filter($parameter) {
		$para = array();
		while (list ($key, $val) = each ($parameter)) {
			if($key == "sign" || $key == "sign_type" || $val == "")continue;
			else	$para[$key] = $parameter[$key];

		}
		return $para;
	}
	
	public function arg_sort($array) {
		ksort($array);
		reset($array);
		return $array;
	}

	public function charset_encode($input,$_output_charset ,$_input_charset ="GBK" ) {
		$output = "";
		if($_input_charset == $_output_charset || $input ==null) {
			$output = $input;
		} elseif (function_exists("mb_convert_encoding")){
			$output = mb_convert_encoding($input,$_output_charset,$_input_charset);
		} elseif(function_exists("iconv")) {
			$output = iconv($_input_charset,$_output_charset,$input);
		} else $this->getResponse()->setBody("sorry, you have no libs support for charset change.");
		return $output;
	}
   
	
    /**
     * Set Billing Address data
     *
     * @param $formFields
     */
    protected function setBillingAddressData($formFields)
    {
        $billingAddress = $this->_order->getBillingAddress();

        if ($billingAddress) {

            $formFields['shopper.firstName'] = trim($billingAddress->getFirstname());
            $middleName = trim($billingAddress->getMiddlename());
            if ($middleName != "") {
                $formFields['shopper.infix'] = trim($middleName);
            }

            $formFields['shopper.lastName'] = trim($billingAddress->getLastname());
            $formFields['shopper.telephoneNumber'] = trim($billingAddress->getTelephone());

            $street = $this->_sunHelper->getStreet($billingAddress);

            if (isset($street['name']) && $street['name'] != "") {
                $formFields['billingAddress.street'] = $street['name'];
            }

            if (isset($street['house_number']) && $street['house_number'] != "") {
                $formFields['billingAddress.houseNumberOrName'] = $street['house_number'];
            } else {
                $formFields['billingAddress.houseNumberOrName'] = "NA";
            }

            if (trim($billingAddress->getCity()) == "") {
                $formFields['billingAddress.city'] = "NA";
            } else {
                $formFields['billingAddress.city'] = trim($billingAddress->getCity());
            }

            if (trim($billingAddress->getPostcode()) == "") {
                $formFields['billingAddress.postalCode'] = "NA";
            } else {
                $formFields['billingAddress.postalCode'] = trim($billingAddress->getPostcode());
            }

            if (trim($billingAddress->getRegionCode()) == "") {
                $formFields['billingAddress.stateOrProvince'] = "NA";
            } else {
                $formFields['billingAddress.stateOrProvince'] = trim($billingAddress->getRegionCode());
            }

            if (trim($billingAddress->getCountryId()) == "") {
                $formFields['billingAddress.country'] = "NA";
            } else {
                $formFields['billingAddress.country'] = trim($billingAddress->getCountryId());
            }
        }
        return $formFields;
    }

    /**
     * Set Shipping Address data
     *
     * @param $formFields
     */
    protected function setShippingAddressData($formFields)
    {
        $shippingAddress = $this->_order->getShippingAddress();

        if ($shippingAddress) {

            $street = $this->_sunHelper->getStreet($shippingAddress);

            if (isset($street['name']) && $street['name'] != "") {
                $formFields['deliveryAddress.street'] = $street['name'];
            }

            if (isset($street['house_number']) && $street['house_number'] != "") {
                $formFields['deliveryAddress.houseNumberOrName'] = $street['house_number'];
            } else {
                $formFields['deliveryAddress.houseNumberOrName'] = "NA";
            }

            if (trim($shippingAddress->getCity()) == "") {
                $formFields['deliveryAddress.city'] = "NA";
            } else {
                $formFields['deliveryAddress.city'] = trim($shippingAddress->getCity());
            }

            if (trim($shippingAddress->getPostcode()) == "") {
                $formFields['deliveryAddress.postalCode'] = "NA";
            } else {
                $formFields['deliveryAddress.postalCode'] = trim($shippingAddress->getPostcode());
            }

            if (trim($shippingAddress->getRegionCode()) == "") {
                $formFields['deliveryAddress.stateOrProvince'] = "NA";
            } else {
                $formFields['deliveryAddress.stateOrProvince'] = trim($shippingAddress->getRegionCode());
            }

            if (trim($shippingAddress->getCountryId()) == "") {
                $formFields['deliveryAddress.country'] = "NA";
            } else {
                $formFields['deliveryAddress.country'] = trim($shippingAddress->getCountryId());
            }
        }
        return $formFields;
    }

    /**
     * @param $formFields
     * @return mixed
     */
    protected function setOpenInvoiceData($formFields)
    {
        $count = 0;
        $currency = $this->_order->getOrderCurrencyCode();

        foreach ($this->_order->getAllVisibleItems() as $item) {

            ++$count;
            $linename = "line".$count;
            $formFields['openinvoicedata.' . $linename . '.currencyCode'] = $currency;
            $formFields['openinvoicedata.' . $linename . '.description'] =
                str_replace("\n", '', trim($item->getName()));

            $formFields['openinvoicedata.' . $linename . '.itemAmount'] =
                $this->_sunHelper->formatAmount($item->getPrice(), $currency);

            $formFields['openinvoicedata.' . $linename . '.itemVatAmount'] =
                ($item->getTaxAmount() > 0 && $item->getPriceInclTax() > 0) ?
                    $this->_sunHelper->formatAmount(
                        $item->getPriceInclTax(),
                        $currency
                    ) -  $this->_sunHelper->formatAmount(
                        $item->getPrice(),
                        $currency
                    ) : $this->_sunHelper->formatAmount($item->getTaxAmount(), $currency);



            // $product = $item->getProduct();

            // Calculate vat percentage
            $percentageMinorUnits = $this->_sunHelper->getMinorUnitTaxPercent($item->getTaxPercent());
            $formFields['openinvoicedata.' . $linename . '.itemVatPercentage'] = $percentageMinorUnits;
            $formFields['openinvoicedata.' . $linename . '.numberOfItems'] = (int) $item->getQtyOrdered();



            if ($this->_order->getPayment()->getAdditionalInformation(
                    \Sunflowerbiz\Alipay\Observer\SunflowerbizHppDataAssignObserver::BRAND_CODE) == "klarna"
            ) {
                $formFields['openinvoicedata.' . $linename . '.vatCategory'] = "High";
            } else {
                $formFields['openinvoicedata.' . $linename . '.vatCategory'] = "None";
            }
        }

        $formFields['openinvoicedata.refundDescription'] = "Refund / Correction for ".$formFields['merchantReference'];
        $formFields['openinvoicedata.numberOfLines'] = $count;

        return $formFields;
    }

    /**
     * @param $genderId
     * @return string
     */
    protected function getGenderText($genderId)
    {
        $result = "";
        if ($genderId == '1') {
            $result = 'MALE';
        } elseif ($genderId == '2') {
            $result = 'FEMALE';
        }
        return $result;
    }

    /**
     * @param null $date
     * @param string $format
     * @return mixed
     */
    protected function _getDate($date = null, $format = 'Y-m-d H:i:s')
    {
        if (strlen($date) < 0) {
            $date = date('d-m-Y H:i:s');
        }
        $timeStamp = new \DateTime($date);
        return $timeStamp->format($format);
    }


    /**
     * The character escape function is called from the array_map function in _signRequestParams
     *
     * @param $val
     * @return mixed
     */
    protected function escapeString($val)
    {
        return str_replace(':', '\\:', str_replace('\\', '\\\\', $val));
    }

    /**
     * Get frontend checkout session object
     *
     * @return \Magento\Checkout\Model\Session
     */
    protected function _getCheckout()
    {
        return $this->_checkoutSession;
    }
}