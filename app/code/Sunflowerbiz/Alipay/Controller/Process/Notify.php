<?php

namespace Sunflowerbiz\Alipay\Controller\Process;

class Notify extends \Magento\Framework\App\Action\Action 
{

    /**
     * @var \Magento\Quote\Model\Quote
     */
    protected $_quote = false;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var \Magento\Sales\Model\Order
     */
    protected $_order;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;
    protected $_scopeConfig;
    protected $_orderHistoryFactory;


    /**
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context
    ) {
	
		
        parent::__construct($context);
		
    }

    /**
     * Return checkout session object
     *
     * @return \Magento\Checkout\Model\Session
     */
    protected function _getCheckoutSession()
    {
        return $this->_checkoutSession;
    }

    /**
     * Set redirect
     */
    public function execute()
    {
	
	 $base =str_replace(array('/app/code/Sunflowerbiz/Alipay/Controller/Process','\app\code\Sunflowerbiz\Alipay\Controller\Process'),'',dirname(__FILE__));
	 
		

      $this->_orderHistoryFactory =  $this->_objectManager->get('\Magento\Sales\Model\Order\Status\HistoryFactory');
	 	$partner_id=   $this->_objectManager->create('Sunflowerbiz\Alipay\Helper\Data')->getConfig('payment/alipaypayment/partner_id');
		$security_code= $this->_objectManager->create('Sunflowerbiz\Alipay\Helper\Data')->getConfig('payment/alipaypayment/security_code');
		$active_log_stock_update = $this->_objectManager->create('Sunflowerbiz\Alipay\Helper\Data')->getConfig('payment/alipaypayment/enable_log');
		$order_status_payment_accepted = $this->_objectManager->create('Sunflowerbiz\Alipay\Helper\Data')->getConfig('payment/alipaypayment/order_status_payment_accepted');
 					
	
		 $notify_type = $this->getRequest()->getParam('notify_type') ;
  		  $subject = $this->getRequest()->getParam('subject') ;			
		if($active_log_stock_update){
			if( $dumpFile = @fopen($base .'/var/log/AliPay.log', 'a+')){
							 fwrite($dumpFile, "\r\n".date("Y-m-d H:i:s").' : Notify data from AliPay : '."\r\n");
		 					fwrite($dumpFile, $notify_type.'->'.$subject." ; ");
							if(isset($_SERVER['REQUEST_URI']))
		 					fwrite($dumpFile, 'Notify URI->'.$_SERVER['REQUEST_URI']." ; "."\r\n");
					}		
		 }
		  
		 if(isset($notify_type) && $notify_type=='trade_status_sync'){
		  
		 	$incrementId=$subject;
		
			$comment="Payment Done.";

			$objManager = \Magento\Framework\App\ObjectManager::getInstance();
			$order = $objManager->create('\Magento\Sales\Model\Order')->loadByIncrementId($incrementId);
			$currentStatus= $order->getStatus();
			if($order_status_payment_accepted!=$currentStatus){
				$order->setState($order_status_payment_accepted)->setStatus($order_status_payment_accepted);
				$order->setTotalPaid($order->getGrandTotal());  
				$order->save();
	
				 $history = $this->_orderHistoryFactory->create()
				 ->setStatus($order_status_payment_accepted)
				->setComment($comment)
				->setEntityName('order')
				->setOrder($order)
				 ;
				 $history->save();
			}
			
			$this->getResponse()->setBody("success");
			return;
		 }
		

    }

    /**
     * Get order object
     *
     * @return \Magento\Sales\Model\Order
     */
    protected function _getOrder()
    {
        if (!$this->_order) {
            $incrementId = $this->_getCheckout()->getLastRealOrderId();
            $this->_orderFactory = $this->_objectManager->get('Magento\Sales\Model\OrderFactory');
            $this->_order = $this->_orderFactory->create()->loadByIncrementId($incrementId);
        }
        return $this->_order;
    }

    /**
     * @return \Magento\Checkout\Model\Session
     */
    protected function _getCheckout()
    {
        return $this->_objectManager->get('Magento\Checkout\Model\Session');
    }

    /**
     * @return mixed
     */
    protected function _getQuote()
    {
        return $this->_objectManager->get('Magento\Quote\Model\Quote');
    }

    /**
     * @return mixed
     */
    protected function _getQuoteManagement()
    {
        return $this->_objectManager->get('\Magento\Quote\Model\QuoteManagement');
    }
}