<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Sunflowerbiz\Alipay\Model;



/**
 * Pay In Store payment method model
 */
class AlipayPayment extends \Magento\Payment\Model\Method\AbstractMethod
{

	 const CODE       = 'alipaypayment';
    /**
     * Payment code
     *
     * @var string
     */
    protected $_code = 'alipaypayment';

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_isOffline = true;

protected $_isInitializeNeeded = true;
  

}
