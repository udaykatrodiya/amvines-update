<?php
namespace Sunflowerbiz\Alipay\Model\Ui;


use Magento\Store\Model\Store as Store;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\Locale\ResolverInterface;
use Magento\Customer\Helper\Session\CurrentCustomer;
use Magento\Payment\Helper\Data as PaymentHelper;
/**
 * Class ConfigProvider
 */
final class SunAlipayConfigProvider implements ConfigProviderInterface
{
    const CODE = 'alipaypayment';

    protected $method;
    protected $_urlBuilder;
    protected $config;
	protected $_request;
	protected $_paymentHelper;


    public function __construct(
        PaymentHelper $paymentHelper,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\UrlInterface $urlBuilder,
        Store $store
    )
    {
        $this->method[self::CODE] = $paymentHelper->getMethodInstance(self::CODE);
        $this->store = $store;
		 $this->_paymentHelper = $paymentHelper;
		  $this->_request = $request;
        $this->_urlBuilder = $urlBuilder;
    }

    /**
     * Retrieve assoc array of checkout configuration
     *
     * @return array
     */
    public function getConfig()
    {
      /*  $merchant_id = $this->method->getConfigData('merchant_id');
        if ($this->method->getConfigData('test_mode')) {
          //  $public_key = $this->method->getConfigData('test_public_api_key');
        }*/

        return [
            'payment' => [
                self::CODE => [
                    'isActive' => true,
                    'redirectUrl' =>$this->_urlBuilder->getUrl('alipay/process/redirect')
                ]
            ]
        ];
    }
	
	 protected function _getRequest()
    {
        return $this->_request;
    }
}
