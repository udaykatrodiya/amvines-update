<?php

namespace Sunflowerbiz\ChinaSocialLogin\Controller\Process;

class Sociallogin extends \Magento\Framework\App\Action\Action 
{
	protected $storeManager;
	protected $customerFactory;
	protected $customerRepository;
	protected $searchCriteriaBuilder;
	protected $filterBuilder;
	protected $ustomerSession;
	protected $messageManager;
    public function __construct(
        \Magento\Framework\App\Action\Context $context
    ) {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$this->storeManager     =  $objectManager->create('\Magento\Store\Model\StoreManagerInterface');
        $this->customerFactory  =  $objectManager->create('\Magento\Customer\Model\CustomerFactory');
        $this->customerRepository  = $objectManager->create('\Magento\Customer\Api\CustomerRepositoryInterface');
        $this->searchCriteriaBuilder  =$objectManager->create('\Magento\Framework\Api\SearchCriteriaBuilder');
        $this->filterBuilder  = $objectManager->create('\Magento\Framework\Api\FilterBuilder');
		$this->customerSession = $objectManager->create('Magento\Customer\Model\Session');
		$this->messageManager = $objectManager->create('\Magento\Framework\Message\ManagerInterface');
        parent::__construct($context);
    }

  
    /**
     * Set redirect
     */
    public function execute()
    {
	
	
	
	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
	$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	$core_write = $resource->getConnection();
	$urlInterface = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\UrlInterface');
     $baseurl = $urlInterface->getBaseUrl().'chinasociallogin/process/sociallogin';
	 
	$connect = $this->getRequest()->getParam('connect') ;
	 $action = $this->getRequest()->getParam('action',false);
	$access = $this->getRequest()->getParam('access',false);	
	$return = $this->getRequest()->getParam('return',false);		
	$code = $this->getRequest()->getParam('code',false);	
	

	if($return!="")
	$this->customerSession->setData('chinasociallogin_return',$return);
	
	$redirectpage= $this->customerSession->getData('chinasociallogin_return');
		
     $currenturl = $urlInterface->getCurrentUrl();

	$wechat_app_id =  $objectManager->create('Sunflowerbiz\ChinaSocialLogin\Helper\Data')->getConfig('se_chinasociallogin/chinasociallogin/wechat_app_id');
	$wechat_app_secret =  $objectManager->create('Sunflowerbiz\ChinaSocialLogin\Helper\Data')->getConfig('se_chinasociallogin/chinasociallogin/wechat_app_secret');
	
	$qq_app_id =  $objectManager->create('Sunflowerbiz\ChinaSocialLogin\Helper\Data')->getConfig('se_chinasociallogin/chinasociallogin/qq_app_id');
	$qq_app_key =  $objectManager->create('Sunflowerbiz\ChinaSocialLogin\Helper\Data')->getConfig('se_chinasociallogin/chinasociallogin/qq_app_key');
	
	$sina_app_key =  $objectManager->create('Sunflowerbiz\ChinaSocialLogin\Helper\Data')->getConfig('se_chinasociallogin/chinasociallogin/sina_app_key');
	$sina_app_secret =  $objectManager->create('Sunflowerbiz\ChinaSocialLogin\Helper\Data')->getConfig('se_chinasociallogin/chinasociallogin/sina_app_secret');
	
	$taobao_app_key =  $objectManager->create('Sunflowerbiz\ChinaSocialLogin\Helper\Data')->getConfig('se_chinasociallogin/chinasociallogin/taobao_app_key');
	$taobao_app_secret =  $objectManager->create('Sunflowerbiz\ChinaSocialLogin\Helper\Data')->getConfig('se_chinasociallogin/chinasociallogin/taobao_app_secret');
		

		define('WECHAT_AKEY',$wechat_app_id);
		define('WECHAT_SKEY',$wechat_app_secret);
		
		define('QQ_AKEY',$qq_app_id);
		define('QQ_SKEY',$qq_app_key);
		
		define('SINA_AKEY',$sina_app_key);
		define('SINA_SKEY', $sina_app_secret);
		define('TAOBAO_AKEY',$taobao_app_key);
		define('TAOBAO_SKEY',$taobao_app_secret);

		if($connect=='qq' && !$action){
			$params=array(
			'response_type'=>'code',
			'client_id'=>QQ_AKEY,
			'state'=>md5(uniqid(rand(), true)),
			'scope'=>'get_user_info,add_share,list_album,add_album,upload_pic,add_topic,add_one_blog,add_weibo',
			'redirect_uri'=>$baseurl.'?connect=qq&action=callback'//home_url('/').'?connect=qq&action=callback'
		);
			$url=('https://graph.qq.com/oauth2.0/authorize?'.http_build_query($params));
			//echo $params['redirect_uri'].$url;die();
			 return $this->redirect($url);
			}
		if($connect=='qq' && $action=='callback'){	
		
		$params=array(
			'grant_type'=>'authorization_code',
			'code'=>$code,
			'client_id'=>QQ_AKEY,
			'client_secret'=>QQ_SKEY,
			'redirect_uri'=>$baseurl.'?connect=qq&action=callback&access=1'
		);
		$str = $this->open_connect_http('https://graph.qq.com/oauth2.0/token?'.http_build_query($params));
		
		$access_token = $str['access_token'];
		$str_r = $this->open_connect_http("https://graph.qq.com/oauth2.0/me?access_token=".$access_token);
		//echo '<pre>';print_r($str_r);echo '</pre>';
		if(isset($str_r['error'])) return;
		$openid =  $str_r['openid'];
		//$openid='testqq';
		 if ($openid) {
		 	$userInfo = $this->open_connect_http('https://graph.qq.com/user/get_user_info?access_token='.$access_token.'&oauth_consumer_key='.QQ_AKEY.'&openid='.$openid);
		//	$userInfo =array('nickname'=>'testqqnickname');
			
            $this->checkRegisterStatusAndLogin('qq',$openid,$userInfo);
			
			return $this->redirect($redirectpage);
			
        }
		return;
		}
		
		if($connect=='sina' && !$action){
		
			$params=array(
			'response_type'=>'code',
			'client_id'=>SINA_AKEY,
			'forcelogin'=>'true',
			'redirect_uri'=>$baseurl.'?connect=sina&action=callback'//home_url('/').'?connect=qq&action=callback'
		);
			$url=('https://api.weibo.com/oauth2/authorize?'.http_build_query($params));
			//echo $params['redirect_uri'].$url;die();
			 return $this->redirect($url);
			}
		if($connect=='sina' && $action=='callback'){	
		
		$params=array(
			'grant_type'=>'authorization_code',
			'code'=>$code,
			'client_id'=>SINA_AKEY,
			'client_secret'=>SINA_SKEY,
			'redirect_uri'=>$baseurl.'?connect=sina&action=callback&access=1'
		);
			
		$str_r = $this->open_connect_http('https://api.weibo.com/oauth2/access_token?'.http_build_query($params),'', 'POST');
	//	echo 'https://api.weibo.com/oauth2/access_token?'.http_build_query($params);
		//print_r($str_r);
		//echo '<pre>';print_r($str_r);echo '</pre>';
		if(isset($str_r['error'])) return ("<h3>error:</h3>".$str_r['error']."<h3>");
		$openid =  $str_r['uid'];
		$access_token = $str_r["access_token"];

		 if ($openid) {
		 
          	$userInfo = $this->open_connect_http("https://api.weibo.com/2/users/show.json?access_token=".$access_token."&uid=".$openid);
			
            $this->checkRegisterStatusAndLogin('sina',$openid,$userInfo);
			
          return $this->redirect($redirectpage);
        }
		return;
		}
		
		
		
		if($connect=='taobao' && !$action){
			$params=array(
			'response_type'=>'code',
			'client_id'=>TAOBAO_AKEY,
			'forcelogin'=>'true',
			'redirect_uri'=>$baseurl.'?connect=taobao&action=callback'//home_url('/').'?connect=qq&action=callback'
		);
			$url=('https://oauth.taobao.com/authorize?'.http_build_query($params));
			//echo $params['redirect_uri'].$url;die();
			return $this->redirect($url);
			}
		if($connect=='taobao' && $action=='callback'){	
		
		$params=array(
			'grant_type'=>'authorization_code',
			'response_type'=>'code',
			'code'=>$code,
			'client_id'=>SINA_AKEY,
			'client_secret'=>SINA_SKEY,
			'redirect_uri'=>$baseurl.'?connect=taobao&action=callback&access=1'
		);
		$str_r = $this->open_connect_http('https://oauth.taobao.com/token?'.http_build_query($params),'', 'POST');
	
		if(!isset($str_r['error'])) return ("<h3>error:</h3>".$str_r['error']."<h3>");
		$openid =  $str_r['taobao_user_id'];
		$access_token = $str["access_token"];
		 if ($openid) {
			$userInfo=array('nickname'=>$str_r['taobao_user_nick'],'sex'=>'M');
            $this->checkRegisterStatusAndLogin('taobao',$openid,$userInfo);
			return $this->redirect($redirectpage);
        }
		return;
		}
	
		if($connect=='wechat' && !$code){
			
            $url = $this->createOauthUrlForCode($baseurl);
		
           return $this->redirect($url);
        } else {
			
        	 $openid = $this->GetOpenidFromMp($code);
			 $appid =WECHAT_AKEY;
       		 $secret = WECHAT_SKEY;
			 $userinfo = "";
			$url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=$appid&secret=$secret";
			$token = $this->getJson($url);
			 
			$oauth2Url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=$appid&secret=$secret&code=$code&grant_type=authorization_code";
			$oauth2 =  $this->getJson($oauth2Url);
			$access_token = $token["access_token"];  
			if(isset($oauth2['openid']) && $oauth2['openid']!=""){
			$openid2 = $oauth2['openid'];  
			$get_user_info_url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=$access_token&openid=$openid2&lang=zh_CN";
			$userinfo =  $this->getJson($get_user_info_url);
			}
		  	 $this->checkRegisterStatusAndLogin('wechat',$openid,$userinfo);
			 return $this->redirect($redirectpage);
        }
		

 
	
	
	return ;
 
    }
	
	  public function  getJson($url){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($ch);
		curl_close($ch);
		return json_decode($output, true);
	}
	
	function GetUnionid($code){
		  $urlObj["appid"] =WECHAT_AKEY;
       	 $urlObj["secret"] = WECHAT_SKEY;
       	 $urlObj["code"] = $code;
			$urlObj["grant_type"] = "authorization_code";
			$bizString = $this->ToUrlParams($urlObj);
			$url= "https://api.weixin.qq.com/sns/oauth2/access_token?" . $bizString;
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_TIMEOUT, 30);
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_HEADER, FALSE);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			$res = curl_exec($ch);
			curl_close($ch);
			$wxreturn = json_decode($res, true);
			$unionid=$wxreturn['unionid'];
			return $unionid;
	}
	
	  public function GetOpenidFromMp($code){
		$url = $this->_CreateOauthUrlForOpenid($code);
		
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $res = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($res, true);
        //$this->_responseTokenData = $data;
		//print_r($data);
		$openid ='';
		if(isset($data['openid']))
        $openid = $data['openid'];
		
        return $openid;
	}
	
	  public function redirect($url){
		$resultRedirect = $this->resultRedirectFactory->create();
		$resultRedirect->setPath($url);
		return $resultRedirect;
	}
	
    public function _CreateOauthUrlForOpenid($code)
    {
        $urlObj["appid"] =WECHAT_AKEY;
        $urlObj["secret"] = WECHAT_SKEY;
        $urlObj["code"] = $code;
        $urlObj["grant_type"] = "authorization_code";
        $bizString = $this->ToUrlParams($urlObj);
        return "https://api.weixin.qq.com/sns/oauth2/access_token?" . $bizString;
    }


 	public function ToUrlParams($urlObj)
    {
        $buff = "";
        foreach ($urlObj as $k => $v) {
            if ($k != "sign") {
                $buff .= $k . "=" . $v . "&";
            }
        }

        $buff = trim($buff, "&");
        return $buff;
    }
    public function createOauthUrlForCode($redirectUrl)
    {
        $urlObj["appid"] = WECHAT_AKEY;
        $urlObj["redirect_uri"] = "$redirectUrl";
        $urlObj["response_type"] = "code";
        $urlObj["scope"] = "snsapi_login"; // snsapi_base
        $urlObj["state"] = "STATE" . "#wechat_redirect";
        $bizString = $this->ToUrlParams($urlObj);
			
			
			
        return "https://open.weixin.qq.com/connect/qrconnect?" . $bizString;
        //return "https://open.weixin.qq.com/connect/oauth2/authorize?" . $bizString;
    }
	
	  public function createSOauthUrlForCode($redirectUrl)
    {
	
        $urlObj["appid"] = WECHAT_AKEY;
        $urlObj["redirect_uri"] = "$redirectUrl";
        $urlObj["response_type"] = "code";
        $urlObj["scope"] = "snsapi_userinfo"; // snsapi_base
        $urlObj["state"] = "STATE" . "#wechat_redirect";
        $bizString = $this->ToUrlParams($urlObj);
			
        return "https://open.weixin.qq.com/connect/oauth2/authorize?" . $bizString;
        //return "https://open.weixin.qq.com/connect/oauth2/authorize?" . $bizString;
    }

	public function open_connect_http($url, $postfields='', $method='GET', $headers=array()){
		$ci = curl_init();
		curl_setopt($ci, CURLOPT_SSL_VERIFYPEER, false); 
		curl_setopt($ci, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ci, CURLOPT_HEADER, false);
		curl_setopt($ci, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ci, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ci, CURLOPT_TIMEOUT, 30);
		if($method=='POST'){
			curl_setopt($ci, CURLOPT_POST, TRUE);
			if($postfields!='')curl_setopt($ci, CURLOPT_POSTFIELDS, $postfields);
		}
		
		//$headers[] = 'User-Agent: Open Social Login for China(sunflowerbiz.com)';
		//$headers[] = 'Expect:';
		//curl_setopt($ci, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ci, CURLOPT_URL, $url);
		$response = curl_exec($ci);
		if($response===false) $response = curl_error($ci);
		curl_close($ci);
		$response = trim(trim($response),'&&&START&&&');
		$json_r = array();
		$json_r = json_decode($response, true);
		if(count($json_r)==0){
			parse_str($response,$json_r);
			if(count($json_r)==1 && current($json_r)==='') return $response;
		}
		return $json_r;
	}

	public function open_close($open_info){
	}
	
	function checkRegisterStatusAndLogin($site='china',$openid='',$userInfo='')
    {
		
 	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    $filters = [];
  		$filters[] = $this->filterBuilder
            ->setField('social_id')
            ->setConditionType('like')
            ->setValue($openid. '%')
            ->create();
    $this->searchCriteriaBuilder->addFilters($filters);
    $searchCriteria = $this->searchCriteriaBuilder->create();
	
    $searchResults = $this->customerRepository->getList($searchCriteria);
	$uidExist=count($searchResults->getItems());
	
        if ($uidExist) {
			
		 foreach ($searchResults->getItems() as $customer) {
		
			 $CustomerModel = $objectManager->create('Magento\Customer\Model\Customer');
			  $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();
			  $CustomerModel->setWebsiteId($websiteId);
			 $CustomerModel->loadByEmail($customer->getEmail());
		//	 $this->messageManager->addSuccess(__('We create an account for you, Please change your information!'));
           $this->customerSession->setCustomerAsLoggedIn($CustomerModel);
		   return;
		 }
		
			
        } else {
			$default_firstname =  $objectManager->create('Sunflowerbiz\ChinaSocialLogin\Helper\Data')->getConfig('se_chinasociallogin/chinasociallogin/default_firstname');
			$default_lastname =  $objectManager->create('Sunflowerbiz\ChinaSocialLogin\Helper\Data')->getConfig('se_chinasociallogin/chinasociallogin/default_lastname');
			//$default_email =  $objectManager->create('Sunflowerbiz\ChinaSocialLogin\Helper\Data')->getConfig('se_chinasociallogin/chinasociallogin/default_email');
			$default_password =  $objectManager->create('Sunflowerbiz\ChinaSocialLogin\Helper\Data')->getConfig('se_chinasociallogin/chinasociallogin/default_password');
			
			if(!is_array($userInfo)) $userInfo=array();	
			if(!isset($userInfo['nickname'])){
			$userInfo['firstname']=$default_firstname;
			$userInfo['lastname']=$default_lastname;
			}else{
			$userInfo['firstname']= $userInfo['nickname'];
			$userInfo['lastname']= $userInfo['nickname'];
			}

		 $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();
        // Instantiate object (this is the most important part)
        $customer   = $this->customerFactory->create();
        $customer->setWebsiteId($websiteId);

        // Preparing data for new customer
        $customer->setEmail( $openid . '@'.$site.'.com'); 
        $customer->setFirstname( $userInfo['firstname']);
        $customer->setLastname($userInfo['lastname']);
        $customer->setPassword($default_password);
        $customer->setSocialId($openid);

        // Save data
        $customer->save();
		 $this->messageManager->addSuccess(sprintf(__('We create an account for you, default password is %s . Please change your information!'),$default_password));
		$this->customerSession->setCustomerAsLoggedIn($customer);
		
     	 //$customer->sendNewAccountEmail();

           
        }
    }

   
}