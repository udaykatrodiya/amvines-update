<?php

namespace Sunflowerbiz\ChinaSocialLogin\Controller\Process;

class Getsocialicon extends \Magento\Framework\App\Action\Action 
{

    /**
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context
    ) {
	
        parent::__construct($context);
		
    }

  
    /**
     * Set redirect
     */
    public function execute()
    {
	$type = $this->getRequest()->getParam('type') ;		
	$name = $this->getRequest()->getParam('name') ;	
		
	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
	$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	$core_write = $resource->getConnection();

	$wechat_app_id =  $objectManager->create('Sunflowerbiz\ChinaSocialLogin\Helper\Data')->getConfig('se_chinasociallogin/chinasociallogin/wechat_app_id');
	$wechat_app_secret =  $objectManager->create('Sunflowerbiz\ChinaSocialLogin\Helper\Data')->getConfig('se_chinasociallogin/chinasociallogin/wechat_app_secret');
	
	$qq_app_id =  $objectManager->create('Sunflowerbiz\ChinaSocialLogin\Helper\Data')->getConfig('se_chinasociallogin/chinasociallogin/qq_app_id');
	$qq_app_key =  $objectManager->create('Sunflowerbiz\ChinaSocialLogin\Helper\Data')->getConfig('se_chinasociallogin/chinasociallogin/qq_app_key');
	
	$sina_app_key =  $objectManager->create('Sunflowerbiz\ChinaSocialLogin\Helper\Data')->getConfig('se_chinasociallogin/chinasociallogin/sina_app_key');
	$sina_app_secret =  $objectManager->create('Sunflowerbiz\ChinaSocialLogin\Helper\Data')->getConfig('se_chinasociallogin/chinasociallogin/sina_app_secret');
	
	$taobao_app_key =  $objectManager->create('Sunflowerbiz\ChinaSocialLogin\Helper\Data')->getConfig('se_chinasociallogin/chinasociallogin/taobao_app_key');
	$taobao_app_secret =  $objectManager->create('Sunflowerbiz\ChinaSocialLogin\Helper\Data')->getConfig('se_chinasociallogin/chinasociallogin/taobao_app_secret');
		

	$html= "<div class='cnsocial'>";
		if($wechat_app_id!='')
		$html.= "<div class='wechat'><span>".__('微信登录')."</span></div>";
		if($qq_app_id!='')
		$html.=  "<div class='qq'><span>".__('QQ登录')."</span></div>";
		if($sina_app_key!='')
		$html.=  "<div class='sina'><span>".__('微博登录')."</span></div>";
		if($taobao_app_key!='')
		$html.=  "<div class='taobao'><span>".__('淘宝登录')."</span></div>";
		$html.=  "</div>";

	$this->getResponse()->setBody($html);
	return ;
 
    }

   
}