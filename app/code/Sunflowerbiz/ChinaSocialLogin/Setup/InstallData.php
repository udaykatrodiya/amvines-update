<?php

namespace Sunflowerbiz\ChinaSocialLogin\Setup;

 use Magento\Framework\Module\Setup\Migration;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
	 private $customerSetupFactory;


     public function __construct(\Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory)
    {
        $this->customerSetupFactory = $customerSetupFactory;
    }
 	public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
 
        $installer = $setup;
        $installer->startSetup();
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
        $entityTypeId = $customerSetup->getEntityTypeId(\Magento\Customer\Model\Customer::ENTITY);
        $customerSetup->removeAttribute(\Magento\Customer\Model\Customer::ENTITY, "social_id");
 
        $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, "social_id",  array(
            "type"     => "varchar",
            "backend"  => "",
            "label"    => "social_id",
            "input"    => "text",
            "source"   => "",
            "visible"  => true,
            "required" => false,
            "default" => "",
            "frontend" => "",
            "unique"     => false,
            "note"       => ""
 
        ));
        $my_attribute   = $customerSetup->getAttribute(\Magento\Customer\Model\Customer::ENTITY, "social_id");
 
        $my_attribute = $customerSetup->getEavConfig()->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'social_id');
        $used_in_forms[]="adminhtml_customer";
       // $used_in_forms[]="checkout_register";
        //$used_in_forms[]="customer_account_create";
        //$used_in_forms[]="customer_account_edit";
        //$used_in_forms[]="adminhtml_checkout";
        $my_attribute->setData("used_in_forms", $used_in_forms)
            ->setData("is_used_for_customer_segment", true)
            ->setData("is_system", 0)
            ->setData("is_user_defined", 1)
            ->setData("is_visible", 1)
            ->setData("sort_order", 100);
        $my_attribute->save();
        $installer->endSetup();
    }
}
 
