<?php
namespace Tigren\Dailydeal\Controller\Adminhtml\Subscriber\MassDelete;

/**
 * Interceptor class for @see \Tigren\Dailydeal\Controller\Adminhtml\Subscriber\MassDelete
 */
class Interceptor extends \Tigren\Dailydeal\Controller\Adminhtml\Subscriber\MassDelete implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Ui\Component\MassAction\Filter $filter, \Tigren\Dailydeal\Model\ResourceModel\Subscriber\CollectionFactory $collectionFactory)
    {
        $this->___init();
        parent::__construct($context, $filter, $collectionFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
