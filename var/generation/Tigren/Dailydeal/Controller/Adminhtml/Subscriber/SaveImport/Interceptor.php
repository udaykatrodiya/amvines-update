<?php
namespace Tigren\Dailydeal\Controller\Adminhtml\Subscriber\SaveImport;

/**
 * Interceptor class for @see \Tigren\Dailydeal\Controller\Adminhtml\Subscriber\SaveImport
 */
class Interceptor extends \Tigren\Dailydeal\Controller\Adminhtml\Subscriber\SaveImport implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\File\Csv $csvProcessor, \Tigren\Dailydeal\Model\SubscriberFactory $subscriberFactory)
    {
        $this->___init();
        parent::__construct($context, $csvProcessor, $subscriberFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
