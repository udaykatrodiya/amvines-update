<?php
namespace Tigren\Dailydeal\Controller\Adminhtml\Subscriber\Save;

/**
 * Interceptor class for @see \Tigren\Dailydeal\Controller\Adminhtml\Subscriber\Save
 */
class Interceptor extends \Tigren\Dailydeal\Controller\Adminhtml\Subscriber\Save implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\Stdlib\DateTime\DateTime $date)
    {
        $this->___init();
        parent::__construct($context, $date);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
