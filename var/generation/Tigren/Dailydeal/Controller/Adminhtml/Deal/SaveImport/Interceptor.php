<?php
namespace Tigren\Dailydeal\Controller\Adminhtml\Deal\SaveImport;

/**
 * Interceptor class for @see \Tigren\Dailydeal\Controller\Adminhtml\Deal\SaveImport
 */
class Interceptor extends \Tigren\Dailydeal\Controller\Adminhtml\Deal\SaveImport implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\File\Csv $csvProcessor, \Tigren\Dailydeal\Model\DealFactory $dealFactory, \Magento\Catalog\Model\ProductFactory $productFactory, \Magento\Catalog\Model\Product\LinkFactory $linkFactory, \Magento\Catalog\Model\Product\Attribute\Source\Status $productStatus, \Magento\Framework\Stdlib\DateTime\DateTime $date)
    {
        $this->___init();
        parent::__construct($context, $csvProcessor, $dealFactory, $productFactory, $linkFactory, $productStatus, $date);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
