<?php
namespace Tigren\Dailydeal\Controller\Adminhtml\Deal\Save;

/**
 * Interceptor class for @see \Tigren\Dailydeal\Controller\Adminhtml\Deal\Save
 */
class Interceptor extends \Tigren\Dailydeal\Controller\Adminhtml\Deal\Save implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\Stdlib\DateTime\DateTime $date, \Magento\Backend\Helper\Js $jsHelper, \Tigren\Dailydeal\Helper\Data $helper, \Magento\Catalog\Model\ProductFactory $productFactory)
    {
        $this->___init();
        parent::__construct($context, $date, $jsHelper, $helper, $productFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
