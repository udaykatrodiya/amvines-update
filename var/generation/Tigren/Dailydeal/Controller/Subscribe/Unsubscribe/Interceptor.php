<?php
namespace Tigren\Dailydeal\Controller\Subscribe\Unsubscribe;

/**
 * Interceptor class for @see \Tigren\Dailydeal\Controller\Subscribe\Unsubscribe
 */
class Interceptor extends \Tigren\Dailydeal\Controller\Subscribe\Unsubscribe implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Tigren\Dailydeal\Model\SubscriberFactory $subscriberFactory)
    {
        $this->___init();
        parent::__construct($context, $subscriberFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
