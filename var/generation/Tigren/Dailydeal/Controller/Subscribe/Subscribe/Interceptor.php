<?php
namespace Tigren\Dailydeal\Controller\Subscribe\Subscribe;

/**
 * Interceptor class for @see \Tigren\Dailydeal\Controller\Subscribe\Subscribe
 */
class Interceptor extends \Tigren\Dailydeal\Controller\Subscribe\Subscribe implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Tigren\Dailydeal\Model\SubscriberFactory $subscriberFactory, \Tigren\Dailydeal\Helper\Data $dailydealHelper)
    {
        $this->___init();
        parent::__construct($context, $subscriberFactory, $dailydealHelper);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
