<?php
namespace Tigren\Dailydeal\Block\Sidebar;

/**
 * Interceptor class for @see \Tigren\Dailydeal\Block\Sidebar
 */
class Interceptor extends \Tigren\Dailydeal\Block\Sidebar implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Catalog\Block\Product\Context $context, \Tigren\Dailydeal\Model\DealFactory $dealFactory, \Tigren\Dailydeal\Helper\Data $dailydealHelper, \Magento\Framework\Url\Helper\Data $urlHelper, \Magento\Framework\Data\Form\FormKey $formKey, array $data = array())
    {
        $this->___init();
        parent::__construct($context, $dealFactory, $dailydealHelper, $urlHelper, $formKey, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function getImage($product, $imageId, $attributes = array())
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getImage');
        if (!$pluginInfo) {
            return parent::getImage($product, $imageId, $attributes);
        } else {
            return $this->___callPlugins('getImage', func_get_args(), $pluginInfo);
        }
    }
}
